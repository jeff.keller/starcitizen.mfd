
get_cache_value <- function(name, path = cachePath, backupVal = NULL) {
  
  cache_file <- file.path(path, name)
  if (!file.exists(cache_file)) return(backupVal)
  value <- readLines(cache_file)
  if (value == "") return(backupVal)
  return(value)
  
}

write_cache_value <- function(val, name, path = cachePath) {
  
  cache_file <- file.path(path, name)
  
  OpenMFD::msg("Caching: ", name, "=", val, " [", cache_file, "]")
  
  writeLines(as.character(val), con = cache_file)
  
}

