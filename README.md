# About

`OpenMFD` is a FLOSS companion app for space and flight simulator games such as Star Citizen. It allows you to connect a tablet or other device to the PC running the game to create a Multi-Functional Display (MFD) for an even more immersive game experience.

Simply install the companion app on the PC that will run the game and navigate your tablet's browser to the URL shown.

> **Note: This app is very much a work in progress! Contributions are welcome!**

# Installation

Installation will be made simpler at some point, but for now, please follow these steps (accept all default options):

1. Install [R 3.5.2](https://cran.r-project.org/bin/windows/base/) on the Windows PC that will run the game
2. Install [Rtools35](https://cran.r-project.org/bin/windows/Rtools/) as well
3. Open the R console and run:

    ```
    install.packages("devtools", repos = "https://cloud.r-project.org", type = "binary")
    devtools::install_github("jeffkeller87/KeyboardSimulator", force = TRUE, upgrade = TRUE, dependencies = TRUE, type = "binary")
    devtools::install_gitlab("jeff.keller/OpenMFD", force = TRUE, upgrade = TRUE, dependencies = TRUE, type = "binary")
    OpenMFD::start()
    ```
4. On your secondary device, which is typically on the same network as the PC running the game, navigate the browser to the address shown (e.g., `http://192.168.1.7:4242`).
     
     ![App start messages](/images/app-start-messages.png)

# Known Issues

* Only Star Citizen is currently supported
* There is no visual feedback when a button is pressed (the action being performed in-game is the only indication a button was pressed)
* Some keybindings are not yet supported (e.g., `PrntSc`, `PgUp`, `PgDn`, etc.)

# FAQ

### What devices are supported?

**Any device with an Internet browser!**

You'll have the best experience on devices with at least an 8 inch touch screen. You can even use a secondary touch-enabled monitor connected directly to the PC running the game if you like.

### What operating systems are supported?

**You can connect any device with an Internet browser**.

However, when it comes to the PC actually running `OpenMFD`, only Windows is supported at the moment. Star Citizen is supposedly getting a Linux release post launch, and I fully plan on making this app compatible because I prefer to game on Linux.

### How many devices can I connect?

**As many as you like!**

If your PC is powerful enough to run a game like Star Citizen, it should be powerful enough to run more MFDs than even the most intense sim pits would ever need.

### How does it work?

**In short, it simulates keyboard and mouse inputs on the machine running the game.**

This also means that `OpenMFD` won't display any real time information from the game itself. Wouldn't that be awesome though? If you agree, go to Spectrum and let CIG know that you'd like them to expose a game API to enable these sorts of extensions!

### How can I support development?

**Fork this repository and start making improvements!**

Pull Requests are very much encouraged. Layout contributions are most needed at this time (i.e. CSS, Javascript).

### Why the heck did you write this in R?

Heh, well I'm a Data Scientist during the day so R is very familiar to me. There are also many useful R packages available to make something like this possible, which means initial development was fast.

I'm also hopeful that Cloud Imperium Games will listen to the community and expose a game API to enable amazing integrations and customization. There's a wealth of ship, character, and market information to potentially play with, and since R is a statistical language, I think some really cool stuff could be built down the road.

### Do you accept donations?

Not at this time, but I appreciate the thought! If a lot of folks want to I may set something up later.

# Developers

In addition to the regular installation instructions:

1. Install [RStudio](https://www.rstudio.com/products/rstudio/download/#download)
2. Clone this repository
3. Open the R console and run:

    ```
    install.packages(c("packrat","roxygen2"))
    ```

4. Open `OpenMFD.Rproj` with RStudio
5. Install dependencies (this may take a few minutes)

    ```
    packrat::restore()
    ```

**This site is not endorsed by or affiliated with the Cloud Imperium or Roberts Space Industries group of companies. All game content and materials are copyright Cloud Imperium Rights LLC and Cloud Imperium Rights Ltd.. Star Citizen®, Squadron 42®, Roberts Space Industries®, and Cloud Imperium® are registered trademarks of Cloud Imperium Rights LLC. All rights reserved.**
